# Prbl - Pen plotter firmware #
Prbl is a pen plotter firmware for AVR's atmega processors.
This project is based on Grbl and has been stripped down to the essentials.
Forked from grbl-Mega (https://github.com/gnea/grbl-Mega)

Currently only supports the ATMega 2560

## Pen control ##
Asumed is that the pen's height is controlled via a RC servo. The full range of the servo is mapped to the gcode Z position from 0 to 1mm.
