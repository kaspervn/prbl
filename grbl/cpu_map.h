/*
  cpu_map.h - CPU and pin mapping configuration file
  Part of Grbl

  Copyright (c) 2012-2016 Sungeun K. Jeon for Gnea Research LLC

  Grbl is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Grbl is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Grbl.  If not, see <http://www.gnu.org/licenses/>.
*/

/* The cpu_map.h files serve as a central pin mapping selection file for different
   processor types or alternative pin layouts. This version of Grbl supports only the
   Arduino Mega2560. */

#ifndef cpu_map_h
#define cpu_map_h

#ifdef CPU_MAP_2560_ULTIMAINBOARDV2_BOARD // Ultimainboard v2
#include "nuts_bolts.h"

// Serial port interrupt vectors
#define SERIAL_RX USART0_RX_vect
#define SERIAL_UDRE USART0_UDRE_vect

// Define ports and pins
#define DDR(port) DDR##port
#define _DDR(port) DDR(port)
#define PORT(port) PORT##port
#define _PORT(port) PORT(port)
#define PIN(pin) PIN##pin
#define _PIN(pin) PIN(pin)

// Define step pulse output pins.

#define STEP_PORT_0 A
#define STEP_PORT_1 C
#define STEP_PORT_2 C
#define STEP_BIT_0 3
#define STEP_BIT_1 5
#define STEP_BIT_2 2
#define _STEP_BIT(i) STEP_BIT_##i
#define STEP_BIT(i) _STEP_BIT(i)
#define STEP_DDR(i) _DDR(STEP_PORT_##i)
#define _STEP_PORT(i) _PORT(STEP_PORT_##i)
#define STEP_PORT(i) _STEP_PORT(i)
#define STEP_PIN(i) _PIN(STEP_PORT_##i)

// Define step direction output pins.
#define DIRECTION_PORT_0 A
#define DIRECTION_PORT_1 C
#define DIRECTION_PORT_2 C
#define DIRECTION_BIT_0 1
#define DIRECTION_BIT_1 4
#define DIRECTION_BIT_2 1
#define _DIRECTION_BIT(i) DIRECTION_BIT_##i
#define DIRECTION_BIT(i) _DIRECTION_BIT(i)
#define DIRECTION_DDR(i) _DDR(DIRECTION_PORT_##i)
#define _DIRECTION_PORT(i) _PORT(DIRECTION_PORT_##i)
#define DIRECTION_PORT(i) _DIRECTION_PORT(i)
#define DIRECTION_PIN(i) _PIN(DIRECTION_PORT_##i)

// Define stepper driver enable/disable output pin.
#define STEPPER_DISABLE_PORT_0 A
#define STEPPER_DISABLE_PORT_1 C
#define STEPPER_DISABLE_PORT_2 C
#define STEPPER_DISABLE_BIT_0 5
#define STEPPER_DISABLE_BIT_1 6
#define STEPPER_DISABLE_BIT_2 3
#define STEPPER_DISABLE_BIT(i) STEPPER_DISABLE_BIT_##i
#define STEPPER_DISABLE_DDR(i) _DDR(STEPPER_DISABLE_PORT_##i)
#define STEPPER_DISABLE_PORT(i) _PORT(STEPPER_DISABLE_PORT_##i)
#define STEPPER_DISABLE_PIN(i) _PIN(STEPPER_DISABLE_PORT_##i)

// Define homing/hard limit switch input pins and limit interrupt vectors.
#define MIN_LIMIT_PORT_0 A
#define MIN_LIMIT_PORT_1 A
#define MIN_LIMIT_PORT_2 A
#define MIN_LIMIT_BIT_0 0
#define MIN_LIMIT_BIT_1 4
#define MIN_LIMIT_BIT_2 7
#define _MIN_LIMIT_BIT(i) MIN_LIMIT_BIT_##i
#define MIN_LIMIT_BIT(i) _MIN_LIMIT_BIT(i)
#define MIN_LIMIT_DDR(i) _DDR(MIN_LIMIT_PORT_##i)
#define MIN_LIMIT_PORT(i) _PORT(MIN_LIMIT_PORT_##i)
#define MIN_LIMIT_PIN(i) _PIN(MIN_LIMIT_PORT_##i)

// ultimainboard unconnected pins
#define MAX_LIMIT_PORT_0 J
#define MAX_LIMIT_PORT_1 J
#define MAX_LIMIT_PORT_2 J
#define MAX_LIMIT_BIT_0 5
#define MAX_LIMIT_BIT_1 4
#define MAX_LIMIT_BIT_2 3
#define _MAX_LIMIT_BIT(i) MAX_LIMIT_BIT_##i
#define MAX_LIMIT_BIT(i) _MAX_LIMIT_BIT(i)
#define MAX_LIMIT_DDR(i) _DDR(MAX_LIMIT_PORT_##i)
#define MAX_LIMIT_PORT(i) _PORT(MAX_LIMIT_PORT_##i)
#define MAX_LIMIT_PIN(i) _PIN(MAX_LIMIT_PORT_##i)

//  #define LIMIT_INT       PCIE0  // Pin change interrupt enable pin
//  #define LIMIT_INT_vect  PCINT0_vect
//  #define LIMIT_PCMSK     PCMSK0 // Pin change interrupt register
//  #define LIMIT_MASK ((1<<X_LIMIT_BIT)|(1<<Y_LIMIT_BIT)|(1<<Z_LIMIT_BIT)) // All limit bits
#define DISABLE_HW_LIMITS

// Define spindle enable and spindle direction output pins.
// ultimainboard E0 stepper enable and direction
#define SPINDLE_ENABLE_DDR DDRC
#define SPINDLE_ENABLE_PORT PORTC
#define SPINDLE_ENABLE_BIT 0
#define SPINDLE_DIRECTION_DDR DDRL
#define SPINDLE_DIRECTION_PORT PORTL
#define SPINDLE_DIRECTION_BIT 6

#endif

#ifdef CPU_MAP_2560_ULTIMAINBOARDV2_BOARD // Ultimainboard vE1
#include "nuts_bolts.h"

// Serial port interrupt vectors
#define SERIAL_RX USART0_RX_vect
#define SERIAL_UDRE USART0_UDRE_vect

// Define ports and pins
#define DDR(port) DDR##port
#define _DDR(port) DDR(port)
#define PORT(port) PORT##port
#define _PORT(port) PORT(port)
#define PIN(pin) PIN##pin
#define _PIN(pin) PIN(pin)

// Define step pulse output pins.

#define STEP_PORT_0 A
#define STEP_PORT_1 C
#define STEP_PORT_2 C
#define STEP_BIT_0 3
#define STEP_BIT_1 5
#define STEP_BIT_2 2
#define _STEP_BIT(i) STEP_BIT_##i
#define STEP_BIT(i) _STEP_BIT(i)
#define STEP_DDR(i) _DDR(STEP_PORT_##i)
#define _STEP_PORT(i) _PORT(STEP_PORT_##i)
#define STEP_PORT(i) _STEP_PORT(i)
#define STEP_PIN(i) _PIN(STEP_PORT_##i)

// Define step direction output pins.
#define DIRECTION_PORT_0 A
#define DIRECTION_PORT_1 C
#define DIRECTION_PORT_2 C
#define DIRECTION_BIT_0 1
#define DIRECTION_BIT_1 4
#define DIRECTION_BIT_2 1
#define _DIRECTION_BIT(i) DIRECTION_BIT_##i
#define DIRECTION_BIT(i) _DIRECTION_BIT(i)
#define DIRECTION_DDR(i) _DDR(DIRECTION_PORT_##i)
#define _DIRECTION_PORT(i) _PORT(DIRECTION_PORT_##i)
#define DIRECTION_PORT(i) _DIRECTION_PORT(i)
#define DIRECTION_PIN(i) _PIN(DIRECTION_PORT_##i)

// Define stepper driver enable/disable output pin.
#define STEPPER_DISABLE_PORT_0 A
#define STEPPER_DISABLE_PORT_1 C
#define STEPPER_DISABLE_PORT_2 C
#define STEPPER_DISABLE_BIT_0 5
#define STEPPER_DISABLE_BIT_1 6
#define STEPPER_DISABLE_BIT_2 3
#define STEPPER_DISABLE_BIT(i) STEPPER_DISABLE_BIT_##i
#define STEPPER_DISABLE_DDR(i) _DDR(STEPPER_DISABLE_PORT_##i)
#define STEPPER_DISABLE_PORT(i) _PORT(STEPPER_DISABLE_PORT_##i)
#define STEPPER_DISABLE_PIN(i) _PIN(STEPPER_DISABLE_PORT_##i)

// Define homing/hard limit switch input pins and limit interrupt vectors.
#define MIN_LIMIT_PORT_0 A
#define MIN_LIMIT_PORT_1 A
#define MIN_LIMIT_PORT_2 A
#define MIN_LIMIT_BIT_0 0
#define MIN_LIMIT_BIT_1 4
#define MIN_LIMIT_BIT_2 7
#define _MIN_LIMIT_BIT(i) MIN_LIMIT_BIT_##i
#define MIN_LIMIT_BIT(i) _MIN_LIMIT_BIT(i)
#define MIN_LIMIT_DDR(i) _DDR(MIN_LIMIT_PORT_##i)
#define MIN_LIMIT_PORT(i) _PORT(MIN_LIMIT_PORT_##i)
#define MIN_LIMIT_PIN(i) _PIN(MIN_LIMIT_PORT_##i)

// ultimainboard unconnected pins
#define MAX_LIMIT_PORT_0 K
#define MAX_LIMIT_PORT_1 K
#define MAX_LIMIT_PORT_2 K
#define MAX_LIMIT_BIT_0 0 // ultimainboard disonnected
#define MAX_LIMIT_BIT_1 1 // ultimainboard disonnected
#define MAX_LIMIT_BIT_2 3 // ultimainboard disonnected
#define _MAX_LIMIT_BIT(i) MAX_LIMIT_BIT_##i
#define MAX_LIMIT_BIT(i) _MAX_LIMIT_BIT(i)
#define MAX_LIMIT_DDR(i) _DDR(MAX_LIMIT_PORT_##i)
#define MAX_LIMIT_PORT(i) _PORT(MAX_LIMIT_PORT_##i)
#define MAX_LIMIT_PIN(i) _PIN(MAX_LIMIT_PORT_##i)

//  #define LIMIT_INT       PCIE0  // Pin change interrupt enable pin
//  #define LIMIT_INT_vect  PCINT0_vect
//  #define LIMIT_PCMSK     PCMSK0 // Pin change interrupt register
//  #define LIMIT_MASK ((1<<X_LIMIT_BIT)|(1<<Y_LIMIT_BIT)|(1<<Z_LIMIT_BIT)) // All limit bits
#define DISABLE_HW_LIMITS


#define SPINDLE_ENABLE_DDR DDRK
#define SPINDLE_ENABLE_PORT PORTK
#define SPINDLE_ENABLE_BIT 4 // ultimainboard disonnected
#define SPINDLE_DIRECTION_DDR DDRL
#define SPINDLE_DIRECTION_PORT PORTL
#define SPINDLE_DIRECTION_BIT 5 // ultimainboard disonnected

#endif

/*
#ifdef CPU_MAP_CUSTOM_PROC
  // For a custom pin map or different processor, copy and edit one of the available cpu
  // map files and modify it to your needs. Make sure the defined name is also changed in
  // the config.h file.
#endif
*/

#endif
