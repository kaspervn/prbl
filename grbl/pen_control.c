#include "pen_control.h"
#include "stdint.h"
#include "grbl.h"

/*
 * Servo control basics:
 *   --------                          --------
 *  | pulse |                         | pulse |
 * _|       |_________________________|       |________
 *  <----------------40-200 hz------->
 *  <--pw-->
 * Pulse width pw determines servo angle.
 * pw = 1.5ms -> center
 * pw = 1ms   -> fully left
 * pw = 2ms   -> fully right
 */

#define SERVO_PWM_COUNTER_TOP 0x8000 //With prescaler=8 and clk=16Mhz should result in PWM frequency of ~61 hz
#define SERVO_PWM_MIN (uint16_t)((F_CPU/8) * 0.0005)
#define SERVO_PWM_CENTER (uint16_t)((F_CPU/8) * 0.0015)
#define SERVO_PWM_MAX (uint16_t)((F_CPU/8) * 0.002)

void pen_ctrl_init()
{
    //User timer counter 4 in Fast PWM mode (mode 14)
    //Use OC4A as pwm output
    //clock source: clk/8
    TCCR4A = (1 << WGM41) | (1 << COM4A1);
    TCCR4B = (1 << WGM42) | (1 << WGM43) | (1 << CS41);
    TCCR4C = 0;
    ICR4 = SERVO_PWM_COUNTER_TOP;
    pen_ctrl_set_target(255);
    
    DDRH |= (1 << DDH3); //Set OC4A as output
}

void pen_ctrl_set_target(uint8_t height)
{
    OCR4A = SERVO_PWM_MIN + ((uint32_t)height * (SERVO_PWM_MAX - SERVO_PWM_MIN) / 255);
}
