#ifndef PEN_CONTROL
#define PEN_CONTROL

#include "stdint.h"

/* Pen servo control with one of the 16 bit OC PWM outputs
 */

/* Use PB7 with OC1C */

void pen_ctrl_init();

void pen_ctrl_set_target(uint8_t height);

#endif
